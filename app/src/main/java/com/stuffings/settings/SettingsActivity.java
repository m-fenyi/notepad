package com.stuffings.settings;


import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import androidx.appcompat.app.ActionBar;
import androidx.loader.content.CursorLoader;

import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;

import com.stuffings.notepad.Data;
import com.stuffings.notepad.MainEditor;
import com.stuffings.notepad.R;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static com.stuffings.notepad.Data.recentFiles;
import static com.stuffings.notepad.MainEditor.SETTINGS_RESULT;

public class SettingsActivity extends AppCompatPreferenceActivity {
	private static final String TAG = "SettingsActivity";

	private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange(Preference preference, Object value) {
			String stringValue = value.toString();
			preference.setSummary(stringValue);
			return true;
		}
	};

	private static SharedPreferences sharedPreference;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		sharedPreference=getSharedPreferences(MainEditor.PREFS_NAME,MODE_PRIVATE);
	}

	@Override
	public void onBackPressed(){
		super.onBackPressed();
		Data.savePreferences(sharedPreference);
		this.finishActivity(SETTINGS_RESULT);
	}

	@Override
	public boolean onIsMultiPane() {
		return false;
	}

	@Override
	@TargetApi(Build.VERSION_CODES.P)
	public void onBuildHeaders(List<Header> target) {
		loadHeadersFromResource(R.xml.pref_headers, target);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		int itemId = item.getItemId();
		switch (itemId) {
			case android.R.id.home:
				this.finish();
				break;
		}
		return true;
	}

	private static void bindStringPreferenceSummaryToValue(Preference preference) {
		preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

		sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
				sharedPreference.getString(preference.getKey(), ""));
	}

	@TargetApi(Build.VERSION_CODES.P)
	public static class GeneralPreferenceFragment extends PreferenceFragment{
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_general);
			setHasOptionsMenu(true);

			ListPreference lp=(ListPreference) findPreference("Encoding");
			lp.setValue(sharedPreference.getString(lp.getKey(),"UTF-8"));

			SwitchPreference sp=(SwitchPreference) findPreference("FABvis");
			sp.setChecked(sharedPreference.getBoolean(sp.getKey(),true));

			sp=(SwitchPreference) findPreference("lcVis");
			sp.setChecked(sharedPreference.getBoolean(sp.getKey(),true));

			sp=(SwitchPreference) findPreference("ScreenON");
			sp.setChecked(sharedPreference.getBoolean(sp.getKey(),true));

			sp=(SwitchPreference) findPreference("AutoSave");
			sp.setChecked(sharedPreference.getBoolean(sp.getKey(),true));

			bindStringPreferenceSummaryToValue(findPreference("Encoding"));
		}

		@Override
		public void onDestroy(){
			Data.setEncoding(((ListPreference) findPreference("Encoding")).getValue());

			Data.setFABvisibility(((SwitchPreference) findPreference("FABvis")).isChecked());
			Data.setLineCountVisibility(((SwitchPreference) findPreference("lcVis")).isChecked());

			Data.setKeepScreenOn(((SwitchPreference) findPreference("ScreenON")).isChecked());
			Data.setAutoSave(((SwitchPreference) findPreference("AutoSave")).isChecked());

			Data.savePreferences(sharedPreference);
			super.onDestroy();
		}
	}

	@TargetApi(Build.VERSION_CODES.P)
	public static class DocumentPreferenceFragment extends PreferenceFragment {
		EditTextPreference etp;
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_document);
			setHasOptionsMenu(true);

			SwitchPreference sp=(SwitchPreference) findPreference("lastDoc");
			sp.setChecked(sharedPreference.getBoolean(sp.getKey(),false));
			System.out.println(sp.getKey()+" : "+sharedPreference.getBoolean(sp.getKey(),false));

			etp=(EditTextPreference) findPreference("defDir");
			etp.setDefaultValue(Environment.getExternalStorageDirectory().toString());
			etp.setText(sharedPreference.getString(etp.getKey(), Environment.getExternalStorageDirectory().toString()));

			MultiSelectListPreference mslp=(MultiSelectListPreference) findPreference("Recent");

			Object[] recentArray= recentFiles.toArray();
			String[] stringRecentArray=Arrays.copyOf(recentArray,recentArray.length,String[].class);
			stringRecentArray=Data.getFileName(stringRecentArray);

			mslp.setEntries(stringRecentArray);
			mslp.setEntryValues(stringRecentArray);
			mslp.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
				@Override
				public boolean onPreferenceChange(Preference preference, Object newValue) {
					Log.d(TAG,"Change Listener:\n"+preference.toString()+"\n"+newValue.getClass());
					//newValue contains all files to remove
					HashSet<String> fileTab=(HashSet<String>) newValue;
					for(String file:fileTab){
						String recentF = "";
						for(String recent:recentFiles){
							if(recent.contains(file)){
								recentF = recent;
								break;
							}
						}
						recentFiles.remove(recentF);
						Log.d(TAG,"Removing recent: "+file);
					}
					return false;
				}
			});

			Preference pref=findPreference("ClearList");
			pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
				@Override
				public boolean onPreferenceClick(Preference preference) {
					recentFiles.clear();
					return false;
				}
			});

			pref=findPreference("defDirSelect");
			pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
				@Override
				public boolean onPreferenceClick(Preference preference) {
					Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);

					startActivityForResult(intent, 5);
					return true;
				}
			});

			bindStringPreferenceSummaryToValue(findPreference("defDir"));
		}

		public void onActivityResult(int requestCode, int resultCode, Intent data) {
			if(resultCode==RESULT_OK && requestCode==5 && data!=null) {
				File folder = new File(Data.getRealPathFromURI(data.getData()));

				//String[] proj = { MediaStore.Images.Media.DATA };
				//CursorLoader loader = new CursorLoader(SettingsActivity.this, data.getData(), proj, null, null, null);
				//Cursor cursor = loader.loadInBackground();
				//int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				//cursor.moveToFirst();
				//String result = cursor.getString(column_index);
				//cursor.close();

				if(folder.isDirectory()) {
					Log.d(TAG,"New default folder: " + folder.getAbsolutePath());
					Data.defaultDirectory = folder.getAbsolutePath();
					etp.setText(folder.getAbsolutePath());
					etp.setSummary(folder.getAbsolutePath());
				}
			}
		}

		@Override
		public void onDestroy(){
			Data.setOpenLastDocument(((SwitchPreference)findPreference("lastDoc")).isChecked());

			Data.setDefaultDirectory(((EditTextPreference)findPreference("defDir")).getText());

			Data.savePreferences(sharedPreference);
			super.onDestroy();
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			Log.d(TAG,"Option item selected: " + item.getTitle());
			int id = item.getItemId();
			if (id == android.R.id.home) {
				startActivity(new Intent(getActivity(), SettingsActivity.class));
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	}

	@TargetApi(Build.VERSION_CODES.P)
	public static class EditorPreferenceFragment extends PreferenceFragment{
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_editor);
			setHasOptionsMenu(true);

			ListPreference lp=(ListPreference) findPreference("Font");
			lp.setValue(sharedPreference.getString(lp.getKey(),"Sans"));

			bindStringPreferenceSummaryToValue(findPreference("Font"));
		}

		@Override
		public void onDestroy(){
			Data.setFont(((ListPreference)findPreference("Font")).getValue());

			Data.savePreferences(sharedPreference);
			super.onDestroy();
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			Log.d(TAG,"Option item selected: " + item.getTitle());
			int id = item.getItemId();
			if (id == android.R.id.home) {
				startActivity(new Intent(getActivity(), SettingsActivity.class));
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	}

	protected boolean isValidFragment(String fragmentName) {
		return PreferenceFragment.class.getName().equals(fragmentName)
				|| GeneralPreferenceFragment.class.getName().equals(fragmentName)
				|| EditorPreferenceFragment.class.getName().equals(fragmentName)
				|| DocumentPreferenceFragment.class.getName().equals(fragmentName);
	}
}