package com.stuffings.notepad;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Editable;
import android.text.InputType;
import android.text.Layout;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yydcdut.markdown.MarkdownConfiguration;
import com.yydcdut.markdown.MarkdownEditText;
import com.yydcdut.markdown.MarkdownProcessor;
import com.yydcdut.markdown.syntax.edit.EditFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Timer;
import java.util.TimerTask;

import static android.util.TypedValue.COMPLEX_UNIT_SP;

public class NumberedEditText implements ScrollViewListener{

	private MarkdownProcessor markdownProcessor;
	private MarkdownEditText markdownEditor;

	private volatile TextView lineCount;
	private LinearLayout editorLayout;
	private ObservableScrollView scrollEditor,scrollLineCount;
	private Activity context;
	private Timer timer;

	public NumberedEditText(Activity argContext){
		this.context=argContext;

		markdownEditor = new MarkdownEditText(context);
		markdownEditor.setGravity(Gravity.START);
		markdownEditor.setBackgroundColor(Color.TRANSPARENT);
		markdownEditor.setHorizontallyScrolling(false);
		markdownEditor.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_FLAG_CAP_SENTENCES|InputType.TYPE_TEXT_FLAG_MULTI_LINE);
		markdownEditor.setPadding(0,0,0,0);
		markdownProcessor = new MarkdownProcessor(context);
		MarkdownConfiguration.Builder configBuilder = new MarkdownConfiguration.Builder(context);
		configBuilder.setCodeFontColor(Color.BLACK);
		configBuilder.setHorizontalRulesColor(Color.WHITE);
		configBuilder.setHorizontalRulesHeight(5);
		MarkdownConfiguration markdownConfiguration = configBuilder.build();
		markdownProcessor.config(markdownConfiguration);
		markdownProcessor.factory(EditFactory.create());
		markdownProcessor.live(markdownEditor);

		markdownEditor.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(timer!=null)
					timer.cancel();
				if(!Data.isModified)
					Data.isModified=true;
				}

			@Override
			public void afterTextChanged(Editable s) {
				timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						fillLineCount(markdownEditor.getLayout());
					}
				}, 600);
			}
		});

		lineCount=new TextView(context);
		lineCount.setGravity(Gravity.END);
		lineCount.setText("1");
		lineCount.setMinEms(1);

		editorLayout=new LinearLayout(context);
		editorLayout.setOrientation(LinearLayout.HORIZONTAL);
		LinearLayout.LayoutParams llp=new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		editorLayout.setLayoutParams(llp);

		this.setPreferences();

		scrollEditor=new ObservableScrollView(context);
		scrollEditor.setScrollViewListener(this);
		scrollEditor.setSmoothScrollingEnabled(true);
		scrollEditor.setVerticalScrollBarEnabled(true);
		scrollEditor.setHorizontalScrollBarEnabled(false);
		scrollEditor.setFillViewport(true);
		try{
			Field mScrollCacheField = View.class.getDeclaredField("mScrollCache");
			mScrollCacheField.setAccessible(true);
			Object mScrollCache = mScrollCacheField.get(scrollEditor);
			Field scrollBarField = mScrollCache.getClass().getDeclaredField("scrollBar");
			scrollBarField.setAccessible(true);
			Object scrollBar = scrollBarField.get(mScrollCache);
			Method method = scrollBar.getClass().getDeclaredMethod("setVerticalThumbDrawable", Drawable.class);
			method.setAccessible(true);
			method.invoke(scrollBar, argContext.getResources().getDrawable(R.drawable.scrollbar));
		}catch(Exception e){
			e.printStackTrace();
		}

		scrollLineCount=new ObservableScrollView(context);
		scrollLineCount.setScrollViewListener(this);
		scrollLineCount.setSmoothScrollingEnabled(true);
		scrollLineCount.setVerticalScrollBarEnabled(false);
		scrollLineCount.setHorizontalScrollBarEnabled(false);
		scrollLineCount.setFillViewport(true);
	}

	public LinearLayout getView(){
		//scrollEditor.addView(editor);
		scrollEditor.addView(markdownEditor);
		markdownEditor.setPadding(0,0,0,1500);
		scrollLineCount.addView(lineCount);

		editorLayout.addView(scrollLineCount);
		editorLayout.addView(scrollEditor);

		LinearLayout.LayoutParams lllp=new LinearLayout.LayoutParams (
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		lllp.setMargins(0,0,0,0);
		scrollEditor.setLayoutParams(lllp);

		lllp=new LinearLayout.LayoutParams (
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		lllp.setMargins(5,0,8,0);
		scrollLineCount.setLayoutParams(lllp);

		FrameLayout.LayoutParams fllp=new FrameLayout.LayoutParams (
				FrameLayout.LayoutParams.MATCH_PARENT,
				FrameLayout.LayoutParams.MATCH_PARENT);

		markdownEditor.setLayoutParams(fllp);
		//editor.setLayoutParams(fllp);
		lineCount.setLayoutParams(fllp);

		return editorLayout;
	}

	@Override
	public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
		//TODO onScrollChanged when creating lines above 22
		if(scrollView==scrollLineCount){
			scrollEditor.scrollTo(x,y);
		}else if(scrollView==scrollEditor){
			scrollLineCount.scrollTo(x,y);
		}
	}

	public void setPreferences(){
		this.setFont(Data.font);

		this.setLineCountVisibility(Data.lineCountVisibility);

		this.setEditorTextColor(Data.editorTextColor);
		this.setLineCountTextColor(Data.lineCountTextColor);
		this.setBackgroundColor(Data.backgroundColor);

		this.setFontTextSize(Data.editorTextSize);

		this.setKeepScreenOn(Data.keepScreenOn);
		markdownEditor.refreshDrawableState();
	}

	public void setLineCountVisibility(boolean isVisible){
		if(isVisible)
			lineCount.setVisibility(View.VISIBLE);
		else
			lineCount.setVisibility(View.GONE);
	}

	public void setEditorTextColor(int newColor){
		markdownEditor.setTextColor(newColor);
	}

	public void setLineCountTextColor(int newColor){
		lineCount.setTextColor(newColor);
	}

	public void setBackgroundColor(int newColor){
		markdownEditor.setBackgroundColor(newColor);
	}

	public void setFontTextSize(int size){
		markdownEditor.setTextSize(COMPLEX_UNIT_SP,size);
		lineCount.setTextSize(COMPLEX_UNIT_SP,size);
	}

	public void setFont(String font){
		switch(font){
			case "sans":
			case "Sans":
				markdownEditor.setTypeface(Typeface.SANS_SERIF);
				break;
			case "serif":
			case "Serif":
				markdownEditor.setTypeface(Typeface.SERIF);
				break;
			case "monospace":
			case "Monospace":
				markdownEditor.setTypeface(Typeface.MONOSPACE);
				break;
			default:
				break;
		}
	}

	public void setKeepScreenOn(boolean keepScreenOn){
		editorLayout.setKeepScreenOn(keepScreenOn);
	}

	private void fillLineCount(final Layout layout){
		String lcText="1";

		if (layout != null) {
			final int lineCountc = layout.getLineCount();
			final CharSequence text = layout.getText();
			int j=1, endIndex=0;

			for (int i = 0, startIndex = 0; i < lineCountc; i++) {
				try {
					endIndex = layout.getLineEnd(i);
				}catch(IndexOutOfBoundsException e){
					e.printStackTrace();
					endIndex--;//TODO Is there anything better?
				}
				if (startIndex < 0)				startIndex=0;
				if (endIndex > text.length())	endIndex=text.length();
				if (startIndex > endIndex)		endIndex=startIndex;

				if(text.subSequence(startIndex, endIndex).toString().endsWith("\n")){
					j++;
					lcText+="\n"+j;
				}else
					lcText+="\n";
				startIndex = endIndex;
			}
		}
		final String tlcText=lcText;
		lineCount.post(new Runnable() {
						public void run() {
							lineCount.setText(tlcText);
						}
		});
	}

	public boolean saveText(Uri uri){
		return this.saveText(Data.getRealPathFromURI(uri));
	}

	public void loadText(Uri uri){
		this.loadText(Data.getRealPathFromURI(uri));
	}

	public boolean newText(Uri uri){
		return this.newText(Data.getRealPathFromURI(uri));
	}

	public boolean saveText(String filePath){
		Data.isModified=false;
		return FileStream.saveFile(this.context,markdownEditor.getText().toString(),filePath);
	}

	public void loadText(String filePath){
		markdownEditor.setText(FileStream.openFile(this.context,filePath));
		markdownProcessor.live(markdownEditor);


		Data.openedFile=filePath;
		Data.isModified=false;
	}

	public boolean newText(String filePath){
		markdownEditor.setText("");

		lineCount.setText("1");
		Data.openedFile=filePath;
		File newFile=new File(filePath);
		try {
			if(!newFile.exists() && newFile.createNewFile())
				Toast.makeText(context,"New file created.",Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			Toast.makeText(context,"Unable to create file:\n"+filePath,Toast.LENGTH_SHORT).show();
			e.printStackTrace();
			return false;
		}
		Data.isModified=false;
		return true;
	}
}
