package com.stuffings.notepad;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;


public class FileStream {

	public static String openFile(Activity context, String filePath){
		FileInputStream fis;
		InputStreamReader isr;
		BufferedReader br;
		try {
			if(filePath.contains("/"))
				fis=new FileInputStream(filePath);
			else
				fis=new FileInputStream(Data.defaultDirectory+filePath);
			isr=new InputStreamReader(fis,Data.encoding);
			br=new BufferedReader(isr);
		} catch (FileNotFoundException e) {
			Toast.makeText(context,"Unable to find file:\n"+filePath + "\nCheck permissions.",Toast.LENGTH_LONG).show();
			return "";
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return "";
		}

		String dataread="",line;

		try {
			while ((line = br.readLine()) != null) {
				dataread+=line+"\n";
			}
		} catch (IOException e) {
			Toast.makeText(context,"Unable to read file.",Toast.LENGTH_LONG).show();
			return "";
		}
		return dataread;
	}

	@SuppressLint("WorldWriteableFiles")
	public static boolean saveFile(Activity context, String content, String filePath) {
		BufferedWriter bw;

		try {
			File file;
			if(filePath.contains("/"))
				file=new File(filePath);
			else
				file=new File(Data.defaultDirectory+filePath);

			if(!file.isFile())
				file.createNewFile();

			FileOutputStream fos=new FileOutputStream(file);
			OutputStreamWriter osw=new OutputStreamWriter(fos,Data.encoding);
			bw=new BufferedWriter(osw);
		} catch (FileNotFoundException e) {
			Toast.makeText(context,"Unable to find file.\n"+filePath,Toast.LENGTH_LONG).show();
			return false;
		} catch (IOException e) {
			Toast.makeText(context,"Unable to create file.\n"+filePath,Toast.LENGTH_LONG).show();
			return false;
		}

		try {
			bw.write(content);
			bw.close();
		} catch (IOException e) {
			Toast.makeText(context,"Unable to save file.",Toast.LENGTH_LONG).show();
			return false;
		}

		return true;
	}
}
