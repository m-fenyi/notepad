package com.stuffings.notepad;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import static android.view.WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS;

public class MainEditor extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener{
	private final String TAG = "MainEditor";

	public static final int SAVE_FILE=1,OPEN_FILE=2,NEW_FILE=3,SETTINGS_RESULT=4;
	public static final String PREFS_NAME = "preferences";

	private NavigationView recentNavView;
	private FloatingActionButton fableft,fabright;
	private NumberedEditText editor;
	private SharedPreferences prefs;
	private Timer autoSave;

	public String getRealPathFromURI(Uri contentUri) {
		Cursor cursor = null;
		try {
			String[] proj = { MediaStore.Images.Media.RELATIVE_PATH };
			cursor = getContentResolver().query(contentUri,  proj, null, null, null);
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.RELATIVE_PATH);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_drawer);

		int result = 128;
		int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = getResources().getDimensionPixelSize(resourceId);
		}

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
		getWindow().addFlags(FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
		getWindow().setNavigationBarColor(getResources().getColor(R.color.transparent));
		DrawerLayout drawer = findViewById(R.id.drawer_layout);

		Log.e(TAG, "Padding Top: " + result);

		//prefs=getPreferences(MODE_PRIVATE);
		Log.d(TAG, "Get Pref");
		prefs=getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
		
		Log.d(TAG, "Init Pref");
		Data.initialize(prefs);
		
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null)
			actionBar.hide();

		editor=new NumberedEditText(this);
		FrameLayout editorLayout = findViewById(R.id.editorLayout);
		editorLayout.setPadding(0,result,0,0);
		editorLayout.addView(editor.getView());

		drawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				InputMethodManager inputMethodManager = (InputMethodManager) MainEditor.this
						.getSystemService(Activity.INPUT_METHOD_SERVICE);
				View v = MainEditor.this.getCurrentFocus();
				if (v == null) {
					v = new View(MainEditor.this);
				}
				inputMethodManager.hideSoftInputFromWindow(
						v.getWindowToken(),0);
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				InputMethodManager inputMethodManager = (InputMethodManager) MainEditor.this
						.getSystemService(Activity.INPUT_METHOD_SERVICE);
				View v = MainEditor.this.getCurrentFocus();
				if (v == null) {
					v = new View(MainEditor.this);
				}
				inputMethodManager.hideSoftInputFromWindow(
						v.getWindowToken(),0);
			}
		});

		recentNavView = findViewById(R.id.nav_recent);
		NavigationView optionNavView = findViewById(R.id.nav_option);
		recentNavView.setNavigationItemSelectedListener(this);
		optionNavView.setNavigationItemSelectedListener(this);

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		Log.d(TAG, "Apply Pref");
		if (getIntent().getData() !=null) {
			Log.w("----INTENT----", getIntent().getData().getPath());
			addRecentItem(getRealPathFromURI(getIntent().getData()));
		}

		if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
					999);
		}

		applyPreferences(true);
	}

	@Override
	public void onPause(){
		Data.savePreferences(prefs);
		super.onPause();
	}

	@Override
	public void onDestroy(){
		Data.savePreferences(prefs);
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else if (drawer.isDrawerOpen(GravityCompat.END)) {
			drawer.closeDrawer(GravityCompat.END);
		} else {
			super.onBackPressed();
		}
	}

	public void safetySave(final String file, final int intent, @Nullable final String newFile){
		AlertDialog.Builder adb=new AlertDialog.Builder(this);

		adb.setCancelable(false);
		adb.setTitle("Unsaved changes!");
		adb.setMessage(Data.getFileName(file)+" has not been saved.\nDo you want to save the modifications ?");

		adb.setPositiveButton("Save", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				editor.saveText(file);

				switch(intent){
					case NEW_FILE:
						startNewFileIntent();
						break;
					case OPEN_FILE:
						startOpenFileIntent();
						break;
					default:
						editor.loadText(newFile);
						addRecentItem(newFile);
						break;
				}
			}
		});
		adb.setNegativeButton("Don't Save", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch(intent){
					case NEW_FILE:
						startNewFileIntent();
						break;
					case OPEN_FILE:
						startOpenFileIntent();
						break;
					default:
						editor.loadText(newFile);
						addRecentItem(newFile);
						break;
				}
			}
		});
		adb.create().show();
	}

	private void startNewFileIntent(){
		Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TITLE,"New file.txt");

		startActivityForResult(intent, NEW_FILE);
	}

	private void startOpenFileIntent(){
		Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		intent.setType("text/plain");

		startActivityForResult(intent, OPEN_FILE);
	}

	private void startSaveAsIntent(){
		Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TITLE,Data.getOpenedFile());

		startActivityForResult(intent, SAVE_FILE);
	}

	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		int id=item.getItemId();

		if(id==R.id.optNew){
			if(Data.isModified)
				safetySave(Data.openedFile,NEW_FILE,null);
			else
				startNewFileIntent();
		}else if(id==R.id.optOpen){
			if(Data.isModified)
				safetySave(Data.openedFile,OPEN_FILE,null);
			else
				startOpenFileIntent();
		}else if(id==R.id.optSave) {
			if (Data.openedFile != null && !Data.openedFile.equals("")){
				editor.saveText(Data.openedFile);
				Toast.makeText(this, "File Saved", Toast.LENGTH_SHORT).show();
			}else
				startSaveAsIntent();
		}else if(id==R.id.optSaveAs){
			startSaveAsIntent();
		}else if(id==R.id.optSettings){
			Intent intent = new Intent(this, com.stuffings.settings.SettingsActivity.class);
			startActivityForResult(intent,SETTINGS_RESULT);
		}else if(id==R.id.optExit){
			Data.savePreferences(prefs);
			this.finishAndRemoveTask();
		}else {//Recent File Selection
			for (String recentFile : Data.recentFiles) {
				if (recentFile.contains(item.getTitle())) {
					if(Data.isModified)
						safetySave(Data.openedFile,-1,(recentFile.startsWith("/") ? "" : Data.defaultDirectory) + recentFile);
					else{
						editor.loadText((recentFile.startsWith("/") ? "" : Data.defaultDirectory) + recentFile);
						addRecentItem(recentFile);
					}
					break;
				}
			}
		}
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawers();
		return true;
	}

	private boolean addRecentItem(Uri uri) {
		return this.addRecentItem(Data.getRealPathFromURI(uri));
	}

	private boolean addRecentItem(String item) {
		boolean containsItem = false;
		for (String recentFile : Data.recentFiles) {
			if (recentFile.contains(item)) {
				containsItem = true;
				if(item.contains("/")) {
					Data.recentFiles.remove(item);
					Data.recentFiles.addFirst(item);
				}else{
					Data.recentFiles.remove(Data.defaultDirectory + item);
					Data.recentFiles.addFirst(Data.defaultDirectory + item);
				}
				break;
			}
		}
		if(!containsItem){
			if(item.contains("/")) {
				Data.recentFiles.addFirst(item);
			}else{
				Data.recentFiles.addFirst(Data.defaultDirectory+item);
			}
		}
		fillRecentNav();
		return true;
	}

	private void applyPreferences(boolean startup){
		Log.d(TAG, "Fill recent nav");
		fillRecentNav();
		Log.d(TAG, "Editor Set Pref");
		editor.setPreferences();
		
		Log.d(TAG, "Open last doc");
		if(Data.openLastDocument && startup)
			if(Data.recentFiles.size() > 0 && Data.recentFiles.getFirst() != null)
				editor.loadText(Data.recentFiles.get(0));

		if(Data.autoSave) {
			autoSave=new Timer();
			autoSave.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					if (Data.openedFile != null && !Data.openedFile.equals(""))
						editor.saveText(Data.openedFile);
				}
			}, 0, 300000);
		}else
			if(autoSave!=null)
				autoSave.cancel();

		Data.savePreferences(prefs);
	}

	private void fillRecentNav(){
		Menu recentMenu=recentNavView.getMenu();
		recentMenu.clear();

		for (String recentFile : Data.recentFiles) {
			Log.d(TAG, "Adding to recent: " + recentFile);
			recentMenu.add(recentFile.substring(recentFile.lastIndexOf("/") + 1, recentFile.length()));
			recentMenu.getItem(recentMenu.size()-1).setIcon(R.drawable.ic_recent_file);
		}
		Data.savePreferences(prefs);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode==RESULT_OK) {
			switch(requestCode){
				case OPEN_FILE:
					Uri uri;
					if (data != null){
						uri = data.getData();
						editor.loadText(uri);
						addRecentItem(uri);
					}
					break;
				case SAVE_FILE:
					if (data != null){
						uri = data.getData();
						if(editor.saveText(uri)) {
							editor.loadText(uri);
							addRecentItem(uri);
							Toast.makeText(this, "File Saved", Toast.LENGTH_SHORT).show();
						}
					}
					break;
				case NEW_FILE:
					if (data != null){
						uri = data.getData();
						if(editor.newText(uri))
							addRecentItem(uri);
					}
					break;
			}
		}else if(requestCode==SETTINGS_RESULT)
			this.applyPreferences(false);
	}
}