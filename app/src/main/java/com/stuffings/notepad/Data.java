package com.stuffings.notepad;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by Dimitri on 10/12/2016.
 */

public class Data {
	private static String TAG = "DATA";
	public static String encoding,defaultDirectory,font,openedFile;
	public static LinkedList<String> recentFiles;
	public static boolean FABvisibility,keepScreenOn,autoSave,lineCountVisibility,openLastDocument,isModified;
	public static int editorTextColor,lineCountTextColor,backgroundColor,editorTextSize;

	protected static void initialize(SharedPreferences preferences){
		Data.encoding=preferences.getString("Encoding","UTF-8");
		Data.defaultDirectory=preferences.getString("defDir", Environment.getExternalStorageDirectory().toString());
		Data.font=preferences.getString("Font","sans");

		List<String> temp = new LinkedList<>();
		temp.addAll(preferences.getStringSet("Recent",new HashSet<String>()));
		Data.recentFiles = new LinkedList<>(Arrays.asList(new String[temp.size()]));
		for (String file:temp) {
			String index = file.substring(0,file.indexOf('@'));
			Data.recentFiles.set(Integer.parseInt(index) ,file.substring(file.indexOf('@')+1));
		}

		Data.FABvisibility=preferences.getBoolean("FABvis",true);
		Data.lineCountVisibility=preferences.getBoolean("lcVis",true);
		Data.keepScreenOn=preferences.getBoolean("ScreenON",false);
		Data.autoSave=preferences.getBoolean("AutoSave",false);
		Data.openLastDocument=preferences.getBoolean("lastDoc",false);

		Data.editorTextColor=preferences.getInt("ETC",Color.argb(255,238,238,238));
		Data.lineCountTextColor=preferences.getInt("LCTC",Color.argb(255,204,204,204));
		Data.backgroundColor=preferences.getInt("Background", Color.argb(255,0,0,0));
		Data.editorTextSize=preferences.getInt("ETS",12);

		Data.isModified=false;
		Data.openedFile="New File.txt";
		Data.printData();
	}

	public static void savePreferences(SharedPreferences preferences){
		Log.d(TAG, "Saving preferences!");
		SharedPreferences.Editor editor = preferences.edit();

		editor.putString("Encoding",Data.encoding);
		editor.putString("defDir",Data.defaultDirectory);
		editor.putString("Font",Data.font);

		Set<String> recent = new HashSet<>();
		Integer index = 0;
		for (String file: recentFiles) {
			recent.add(index + "@" + file);
			index ++;
		}
		editor.putStringSet("Recent", recent);

		editor.putBoolean("FABvis",Data.FABvisibility);
		editor.putBoolean("lcVis",Data.lineCountVisibility);
		editor.putBoolean("ScreenON",Data.keepScreenOn);
		editor.putBoolean("AutoSave",Data.autoSave);
		editor.putBoolean("lastDoc",Data.openLastDocument);

		editor.putInt("ETC",Data.editorTextColor);
		editor.putInt("LCTC",Data.lineCountTextColor);
		editor.putInt("Background",Data.backgroundColor);
		editor.putInt("ETS",Data.editorTextSize);

		editor.apply();
		Log.d(TAG, "Preferences saved!");
	}

	public static void printData(){
		Log.d(TAG, "--------Data--------");
		Log.d(TAG, "Encoding: "+Data.encoding);
		Log.d(TAG, "Default Directory: "+Data.defaultDirectory);
		Log.d(TAG, "Font: "+Data.font);

		Log.d(TAG, "Recent Files: "+Data.recentFiles);

		Log.d(TAG, "FAB Visibility: "+Data.FABvisibility);
		Log.d(TAG, "Line-Count Visibility: "+Data.lineCountVisibility);
		Log.d(TAG, "Keeping screen On: "+Data.keepScreenOn);
		Log.d(TAG, "Auto-Save: "+Data.autoSave);
		Log.d(TAG, "Open Last Document: "+Data.openLastDocument);

		Log.d(TAG, "Editor Text Color: "+Data.editorTextColor);
		Log.d(TAG, "Editor Text Size: "+Data.editorTextSize);
		Log.d(TAG, "Line-Count Text Color: "+Data.lineCountTextColor);
		Log.d(TAG, "Background Color: "+Data.backgroundColor);
	}

	public static void printPreferences(SharedPreferences preferences){
		Log.d(TAG, "-----Preferences-----");
		Log.d(TAG, "Encoding: "+preferences.getString("Encoding","Empty"));
		Log.d(TAG, "defDir: "+preferences.getString("defDir","Empty"));
		Log.d(TAG, "Font: "+preferences.getString("Font","Empty"));

		Log.d(TAG, "Recent: "+preferences.getStringSet("Recent",null));

		Log.d(TAG, "FABvis: "+preferences.getBoolean("FABvis",true));
		Log.d(TAG, "lcVis: "+preferences.getBoolean("lcVis",true));
		Log.d(TAG, "ScreenON: "+preferences.getBoolean("ScreenON",true));
		Log.d(TAG, "AutoSave: "+preferences.getBoolean("AutoSave",true));
		Log.d(TAG, "WordWrap: "+preferences.getBoolean("WordWrap",true));
		Log.d(TAG, "lastDoc: "+preferences.getBoolean("lastDoc",true));

		Log.d(TAG, "ETC: "+preferences.getInt("ETC",0));
		Log.d(TAG, "LCTC: "+preferences.getInt("LCTC",0));
		Log.d(TAG, "Background: "+preferences.getInt("Background",0));
		Log.d(TAG, "ETS: "+preferences.getInt("ETS",0));
	}

	public static String getFileName(String absoluteFile){
		return absoluteFile.substring(absoluteFile.lastIndexOf("/")+1);
	}

	public static String[] getFileName(String[] absoluteFiles){
		String[] filesName=absoluteFiles;
		for(int i=0;i<filesName.length;i++){
			filesName[i]=Data.getFileName(filesName[i]);
		}
		return filesName;
	}

	public static String getRealPathFromURI(Uri contentUri) {
		Log.d(TAG, "Uri : "+contentUri);
		Log.d(TAG, "Uri path : "+contentUri.getPath());

		String result = Environment.getExternalStorageDirectory()+"/";
		if (contentUri.toString().contains("documents")){
			result += "Documents/";
		}
		File file = new File(contentUri.getPath());//create path from uri
		final String[] split = file.getPath().split(":");//split the path.
		if (split.length >1)
			result += split[1];

		Log.d(TAG, "Path : "+result);
		return result;
	}

	public static String getAbsoluteOpenedFile(){
		return Data.openedFile;
	}

	public static String getOpenedFile(){
		return Data.openedFile.substring(Data.openedFile.lastIndexOf("/")+1);
	}

	public static void setEncoding(String encoding) {
		switch(encoding){
			case "US-ASCII":
			case "ISO-8859":
			case "UTF-8":
			case "UTF-16BE":
			case "UTF-16LE":
			case "UTF-16":
				Data.encoding = encoding;
				break;
			default:
				break;
		}
	}

	public static void setDefaultDirectory(String defaultDirectory) {
		File f=new File(defaultDirectory);
		if(f.isDirectory() && f.isAbsolute())
			Data.defaultDirectory = defaultDirectory;
	}
	public static void setFont(String font) {
		switch(font){
			case "sans":
			case "Sans":
			case "serif":
			case "Serif":
			case "monospace":
			case "Monospace":
				Data.font = font;
				break;
			default:
				break;
		}
	}

	public static void setFABvisibility(boolean FABvisibility) {
		Data.FABvisibility = FABvisibility;
	}

	public static void setKeepScreenOn(boolean keepScreenOn) {
		Data.keepScreenOn = keepScreenOn;
	}

	public static void setAutoSave(boolean autoSave) {
		Data.autoSave = autoSave;
	}

	public static void setLineCountVisibility(boolean lineCountVisibility) {
		Data.lineCountVisibility = lineCountVisibility;
	}

	public static void setOpenLastDocument(boolean openLastDocument) {
		Data.openLastDocument = openLastDocument;
	}

	public static void setEditorTextColor(int editorTextColor) {
		Data.editorTextColor = editorTextColor;
	}

	public static void setLineCountTextColor(int lineCountTextColor) {
		Data.lineCountTextColor = lineCountTextColor;
	}

	public static void setBackgroundColor(int backgroundColor) {
		Data.backgroundColor = backgroundColor;
	}

	public static void setEditorTextSize(int editorTextSize) {
		Data.editorTextSize = editorTextSize;
	}
}
