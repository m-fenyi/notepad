package com.stuffings.notepad;

/**
 * Created by Dimitri on 10/12/2016.
 */

public interface ScrollViewListener {
	void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy);
}
